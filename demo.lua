local termkey = require('termkey')
local inspect = require('inspect')

local tk = termkey.new({ spacesymbol = true, ctrlc = true })

if not tk then
        print('Cannot allocate termkey instance')
        return
end

if tk:get_flags()["utf8"] then
        print('Termkey in UTF-8 mode')
elseif tk:getflags()["raw"] then
        print('Termkey in RAW mode')
end

print('\027[?1000h')

local k = tk:waitkey()
while k ~= "eof" and k ~= "error" do
        print(inspect(k))

        if k.ctrl and (k.utf8 == "c" or k.utf8 == "C") then
                return
        elseif not (k.ctrl or k.shift or k.alt) and k.utf8 == "?" then
                print("\027[?1$p")
        end

        k = tk:waitkey()
end
