.SUFFIXES:
.SUFFIXES: .o .c

PKG_CONFIG ?= pkg-config
CFLAGS ?= $(shell $(PKG_CONFIG) --cflags lua)
LDFLAGS ?= $(shell $(PKG_CONFIG) --libs lua)

CFLAGS += -Wall -Werror -Wextra

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man
INSTALL_CMOD ?= $(shell $(PKG_CONFIG) --variable INSTALL_CMOD lua)
INSTALL_LMOD ?= $(shell $(PKG_CONFIG) --variable INSTALL_LMOD lua)

CFLAGS  += -fPIC -shared $(shell $(PKG_CONFIG) --cflags termkey)
LDFLAGS +=               $(shell $(PKG_CONFIG) --libs   termkey)

CC ?= gcc
LD ?= gcc

.PHONY: all clean install

all: termkey.so

clean:
	rm termkey.so wrap-termkey.o

install: termkey.so
	mkdir -p $(DESTDIR)$(INSTALL_CMOD)
	cp termkey.so $(DESTDIR)$(INSTALL_CMOD)

%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $^

termkey.so: wrap-termkey.o
	$(CC) -o $@ $^ $(LDFLAGS) -shared -fPIC
